#!/usr/bin/env bash

SCRIPT_DIR=$(dirname "$(readlink -e "$0")")
chmod +x $SCRIPT_DIR/bin/*
sudo cp $SCRIPT_DIR/bin/* /usr/bin
